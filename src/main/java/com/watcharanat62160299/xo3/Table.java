/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.watcharanat62160299.xo3;

/**
 *
 * @author ACER
 */
public class Table {
    private char[][] table ={{'-','-','-'},{'-','-','-'},{'-','-','-'}} ;
    private Player playerX;
    private Player playerO ;
    private Player currentPlayer ;
    private int lastcol,lastrow,countdraw;
    private Player winner ;
    private Boolean finish = false;
    public Table(Player x, Player o){
        playerX = x;
        playerO = o;
        currentPlayer = x;   
    }
    public void showTable(){
    System.out.println(" 123");
        for (int row = 0; row < table.length; row++) {
            System.out.print(row + 1);
            for (int col = 0; col < table[row].length; col++) {
                System.out.print(table[row][col]);
            }
            System.out.println("");
        }
    }
    public boolean setRowCol(int row,int col){
         if (table[row][col] == '-') {
               table[row][col] = currentPlayer.getName();
               
               this.lastrow = row ;
               this.lastcol = col ;
               return true ;
                
         }
         return false;
    
    }
    public Player getCurrenPlayer(){
        return currentPlayer;
            }
    public void switchPlayer(){
        if(currentPlayer==playerX){
            countdraw++;
            currentPlayer = playerO;
        }else{
            countdraw++;
            currentPlayer=playerX ;
        }
    }
    public void checkWin(){
        checkRow();
        checkCol();
        checkX();
        checkDraw();
    }
    void checkX() {
        for (int row = 0; row < table.length; row++) {
            for (int col = 0; col < table[row].length; col++) {
                if (table[0][0] == currentPlayer.getName()
                        && table[1][1] == currentPlayer.getName()
                        && table[2][2] == currentPlayer.getName()) {
                    finish = true;
                    winner = currentPlayer;
                    setStatWinLose();
                } else if (table[0][2] == currentPlayer.getName()
                        && table[1][1] == currentPlayer.getName()
                        && table[2][0] == currentPlayer.getName()) {
                    finish = true;
                    winner = currentPlayer;
                    setStatWinLose();
                
    }
        }
           }
    }
    void checkDraw(){
        if (countdraw == 9) {
            finish = true;
            winner = null;
        }
    }
         void checkCol() {
        for (int row = 0; row < 3; row++) {
            if (table[row][lastcol] != currentPlayer.getName()) {
                return;
            }
        }
        finish = true;
        winner = currentPlayer;
        setStatWinLose();
    }

    private void setStatWinLose() {
        if(currentPlayer==playerO){
            playerO.win();
            playerX.lose();
        }else{
            playerX.win();
            playerO.lose();
        }
    }

    void checkRow() {
        for (int col = 0; col < 3; col++) {
            if (table[lastrow][col] != currentPlayer.getName()) {
                return;
            }
        }
        finish = true;
        winner = currentPlayer;
        setStatWinLose();
    }
    
     public boolean isFinish(){
         return finish;
     }
     public Player getWinner(){
       return winner ;
    }
    
    
    
    
    
    
    
    
    
}
