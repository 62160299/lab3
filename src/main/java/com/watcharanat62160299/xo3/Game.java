/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.watcharanat62160299.xo3;

import java.util.Scanner;

/**
 *
 * @author ACER
 */
public class Game {
    Player playerX;
    Player playerO;
    Player turn;
    Table table;
    int row,col ;
    Scanner kb = new Scanner(System.in) ;
    public Game(){
        playerX = new Player('X');
        playerO = new Player('O');
        table = new Table(playerX,playerO);
    
    }
    public void showTable(){
        table.showTable();
    }
    
    public void input(){
        while (true) {
            System.out.println("Please input Row Col : ");
            row = kb.nextInt() - 1;
            col = kb.nextInt() - 1;
             if (table.setRowCol(row,col)) {
             break ;    
             }
             System.out.println("Error: table at row and col is not empty");   
        }
    }
    public void showWelcome(){
        System.out.println("Welcome to OX Game");
    }
    public void showTurn(){
        System.out.println(table.getCurrenPlayer().getName()+" Turn");
    }
    
    public void run(){
        this.showWelcome();
        while(true){       
        this.showTable();
        this.showTurn();
        this.input();
        table.checkWin();
        if(table.isFinish()){
        if(table.getWinner()==null){
            this.showTable();
            System.out.println("Draw");
        }else{
            this.showTable();
            System.out.println(table.getWinner().getName()+" Win!!!");
        }
            System.out.println("PLAY AGAIN ?" );
            System.out.println("Y OR N ");
            char input = kb.next().charAt(0);
            if(input=='Y'){
                 newGame();
            }else{
                System.out.println("Bye Bye!!!!");
                break;
                 }
            }
        table.switchPlayer(); 
        
        }
    }
    public void newGame(){
        int rand = ((int)(Math.random()*100))%2 ;
      if(rand==0){  
          table = new Table(playerX,playerO);
      }
      else {
          table = new Table(playerO,playerX);
      }
    }
    
    
}
